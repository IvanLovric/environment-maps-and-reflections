﻿
#pragma once

// pch.h

// Vanity includes
#include "..\..\..\Library\Engine\pch.h"

#include "..\..\..\Library\Engine\Core\App.h"
#include "..\..\..\Library\Engine\Core\Application Factory.h"
#include "..\..\..\Library\Engine\Core\Engine Main.h"

#include "..\..\..\Library\Engine\Core\Common\DirectXHelper.h"
#include "..\..\..\Library\Engine\Core\Common\DeviceResources.h"
#include "..\..\..\Library\Engine\Core\Common\StepTimer.h"

#include "..\..\..\Library\Engine\Core\Content\RendererBase3D.h"
#include "..\..\..\Library\Engine\Core\Content\SampleFpsTextRenderer.h"

// Standard C++ includes
