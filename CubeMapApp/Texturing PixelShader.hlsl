
//
//	Texturing PixelShader.hlsl
//  Defining a sample pixel shader for texturing
//
//  � 2015 Vanity DirectX 11.2 Engine (VXE). Zoraja Consulting d.o.o. All rights reserved.
//

TextureCube Texture : register(t0);

SamplerState Sampler : register(s0);

cbuffer Eye: register(b1)
{
	float3 gEyePosW;
};

SamplerState texSampler
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
};


struct PixelShaderInput
{
	float4 pos : SV_POSITION;
	float3 norm : NORMAL;
	float2 tex : TEXCOORD0;
	float4 color: COLOR;
	float4 pos2 : POSITION;
};

int compareFloat3s(float3 a, float3 b)
{
	if (a.x == b.x && a.y == b.y && a.z == b.z)
		return 1;
	else
		return 0;
}

float4 main(PixelShaderInput input) : SV_TARGET
{
	float4 color = Texture.Sample(texSampler, input.pos2);
	
	return color;
}
