//
//	Renderer3D.cpp
//  Implementing a 3D renderer
//
//  � 2015 Vanity DirectX 11.2 Engine (VXE). Zoraja Consulting d.o.o. All rights reserved.
//

#include "pch.h"

#include "Renderer3D.h"

#include "Environment.h"
#include "Sphere.h"

using namespace vxe;
using namespace std;
using namespace concurrency;

using namespace DirectX;
using namespace Windows::Foundation;

void Renderer3D::CreateDeviceDependentResources()
{
	DebugPrint(string("\t Renderer3D::CreateDeviceDependentResources() ... \n"));

	auto device = m_deviceResources->GetD3DDevice();

	vector<task<void>> tasks;

	// texturing vertex shader object and pushing it in the list of asynchronous tasks
	_texturingvertexshader = make_shared<VertexShader<VertexPositionNormalColorTexture>>();
	tasks.push_back(_texturingvertexshader->CreateAsync(device, L"Texturing VertexShader.cso"));

	// Sphere texturing pixel shader object and pushing it in the list of asynchronous tasks
	_pixelshader = make_shared<PixelShader>();
	tasks.push_back(_pixelshader->CreateAsync(device, L"PixelShader.cso"));

	// Enviroment texturing pixel shader object and pushing it in the list of asynchronous tasks
	_texturingpixelshader = make_shared<PixelShader>();
	tasks.push_back(_texturingpixelshader->CreateAsync(device, L"Texturing PixelShader.cso"));	

	// model of CubeMap and pushing it in the list of asynchronous tasks
	_texturingmodel = make_shared<Environment<VertexPositionNormalColorTexture, unsigned short>>(scale);
	tasks.push_back(_texturingmodel->CreateAsync(device));

	// model of SphereMap and pushing it in the list of asynchronous tasks
	_texturingmodel2 = make_shared<Sphere<VertexPositionNormalColorTexture, unsigned short>>(scale, latitude, longitude);
	tasks.push_back(_texturingmodel2->CreateAsync(device));

	// load texture
	_texture = make_shared<Texture2D>(device, DXGI_FORMAT_R8G8B8A8_UNORM);
	tasks.push_back(_texture->CreateDDSAsync(m_deviceResources->GetD3DDeviceContext(), L"Assets\\VaticanTextureArray.dds"));

	// displays a message to the debug window
	when_all(tasks.begin(), tasks.end()).then([this]() {
		m_loadingComplete = true;
		DebugPrint(string("\t -- A lambda: Loading is complete! \n"));

		// Should wait for loading
		SetTexturing();
	});

	//  world transform object
	_world = make_shared<WorldTransforms>(device);
}

void Renderer3D::SetTexturing()
{
	DebugPrint(string("\t Renderer3D::SetTexturing() ...\n"));

	auto context = m_deviceResources->GetD3DDeviceContext();

	_texture->CreateSamplerState();
	_texture->BindSamplerState(context);
	_texture->CreateShaderResourceView();
	_texture->BindShaderResourceView(context);
}


// Initializes view parameters when the window size changes
void Renderer3D::CreateWindowSizeDependentResources()
{
	DebugPrint(string("\t Renderer3D::CreateWindowSizeDependentResources() ...\n"));

	auto device = m_deviceResources->GetD3DDevice();
	auto context = m_deviceResources->GetD3DDeviceContext();

	_camera = std::make_shared<FirstPersonCamera>(device);

	// Eye is at (0,0,3), looking at point (0,10,0) with the up-vector along the y-axis.
	static const XMVECTORF32 eye = { 0.0f, 0.0f, 3.0f, 0.0f };
	//static const XMVECTORF32 eye = { 0.0f, 15.0f, 20.0f, 0.0f };
	static const XMVECTORF32 at = { 0.0f, 10.0f, 0.0f, 0.0f };
	static const XMVECTORF32 up = { 0.0f, 1.0f, 0.0f, 0.0f };

	_camera->InitializeView(eye, at, up);
	_camera->UpdateView(context);
	_camera->UpdateEye(context);

	// displays a message to the debug window
	Size outputSize = m_deviceResources->GetOutputSize();
	DebugPrint(string("\t\t Width:") + to_string(outputSize.Width) + string("\n"));
	DebugPrint(string("\t\t Height:") + to_string(outputSize.Height) + string("\n"));

	// projection transform
	float r = outputSize.Width / outputSize.Height;
	float fov = 70.0f * XM_PI / 180.0f;
	float n = 0.01f;
	float f = 1000.0f;

	if (r < 1.0f) { fov *= 2.0f; }

	XMFLOAT4X4 orientation = m_deviceResources->GetOrientationTransform3D();
	XMMATRIX orientationMatrix = XMLoadFloat4x4(&orientation);

	_camera->SetProjection(orientationMatrix, fov, r, n, f);
	_camera->UpdateProjection(context);
	_camera->BindProjection(context, ProgrammableStage::VertexShaderStage, 2);
}

// Renders one frame using the vertex and pixel shaders.
void Renderer3D::Render()
{
	// Loading is asynchronous. Only draw geometry after it's loaded.
	if (!m_loadingComplete) { return; }

	auto context = m_deviceResources->GetD3DDeviceContext();
	auto device = m_deviceResources->GetD3DDevice();

	// bind the Vertex Shaders and Pixel Shaders to the pipeline
	_texturingvertexshader->Bind(context);
	_texturingpixelshader->Bind(context);

	// bind world, view and projection transforms to the pipeline
	BindCamera();

	// execute the pipeline 
	Draw(_texturingmodel, _world);

	//Bind Sphere pixel shader
	_pixelshader->Bind(context);
	Draw(_texturingmodel2, _world);	//Sphere

	m_deviceResources->SetRasterizerState();
}

void Renderer3D::ReleaseDeviceDependentResources()
{
	// displays a message to the debug window
	DebugPrint(string("\t Renderer3D::ReleaseDeviceDependentResources() ... \n"));

	m_loadingComplete = false;

	// reset all the resources 
	_texturingvertexshader->Reset(); 
	_texturingpixelshader->Reset(); 
	_pixelshader->Reset();

	_texturingmodel->Reset(); 
	_texturingmodel2->Reset();

	_world->Reset();
	_camera->Reset();
}