
//
//	Texturing PixelShader.hlsl
//  Defining a sample pixel shader for texturing
//
//  � 2015 Vanity DirectX 11.2 Engine (VXE). Zoraja Consulting d.o.o. All rights reserved.
//

TextureCube Texture : register(t0);

SamplerState Sampler : register(s0);

SamplerState texSampler
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
};

cbuffer Eye: register(b1)
{
	float3 gEyePosW;
};


struct PixelShaderInput
{
	float4 pos : SV_POSITION;
	float3 norm : NORMAL;
	float2 tex : TEXCOORD0;
	float4 color: COLOR;
	float4 pos2 : POSITION;
};

int compareFloat3s(float3 a, float3 b)
{
	if (a.x == b.x && a.y == b.y && a.z == b.z)
		return 1;
	else
		return 0;
}

float4 main(PixelShaderInput input) : SV_TARGET
{
	//Find vector looking towards the camera
	float3 toEye = gEyePosW - input.pos2;

	//Normalize vector
	float distToEye = length(toEye);
	toEye /= distToEye;

	//Find incident ray vector
	float3 incident = -toEye;

	//Calculate reflection vector in reference to pixel normal
	float3 reflectionVector = reflect(incident, input.norm);

	//Create new vector to flip y axis - flips image vertically
	float3 newVector = { reflectionVector.x, -reflectionVector.y, reflectionVector.z };

	//Sample texture based on newVector which is derived from reflection vector
	float4 color = Texture.Sample(texSampler, newVector);	
	
	return color;
}