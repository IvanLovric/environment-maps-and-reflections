
#pragma once

//
//	Sphere.h ---------------------------------------------------------------------------------
//  Defining sphere model for environment mapping 
//
//  http://www.braynzarsoft.net/viewtutorial/q16390-20-cube-mapping-skybox
//

#include "pch.h"
#include "..\..\..\Library\Engine\Utilities.h"
#include "..\..\..\Library\Engine\Models\Mesh Base.h"

namespace vxe {

	template <typename T, typename U> class Sphere : public MeshBase<T, U> {};

	template <>
	class Sphere<DirectX::VertexPositionNormalColorTexture, unsigned short> : public MeshBase<DirectX::VertexPositionNormalColorTexture, unsigned short>{

	private:
		float scale;
		int latitude;
		int longitude;

	public:
		Sphere(float scale, int latitude, int longitude) :scale(scale), latitude(latitude), longitude(longitude)
		{}


		// Vertices are hardcoded
		virtual concurrency::task<void> CreateAsync(_In_ ID3D11Device2* device) override
		{
			DebugPrint(std::string("\t Sphere<VertexPosition, unsigned short>::CreateAsync() ...\n"));
			// Right-handed
			int NumSphereVertices = ((latitude - 2) * longitude) + 2;
			int NumSphereFaces = ((latitude - 3)*(longitude)* 2) + (longitude * 2);

			float sphereYaw = 0.0f;
			float spherePitch = 0.0f;

			std::vector<DirectX::VertexPositionNormalColorTexture> vertices(NumSphereVertices);

			DirectX::XMVECTOR currVertPos = DirectX::XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f);

			vertices[0].position.x = 0.0f;
			vertices[0].position.y = 0.0f;
			vertices[0].position.z = 1.0f;

			for (int i = 0; i < latitude - 2; ++i)
			{
				spherePitch = (i + 1) * (DirectX::XM_PI / (latitude - 1));
				DirectX::XMMATRIX Rotationx = DirectX::XMMatrixRotationX(spherePitch);
				for (int j = 0; j < longitude; ++j)
				{
					sphereYaw = j * (DirectX::XM_2PI / (longitude));
					DirectX::XMMATRIX Rotationy = DirectX::XMMatrixRotationZ(sphereYaw);
					currVertPos = DirectX::XMVector3TransformNormal(DirectX::XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f), (Rotationx * Rotationy));
					currVertPos = DirectX::XMVector3Normalize(currVertPos);
					vertices[i*longitude + j + 1].position.x = DirectX::XMVectorGetX(currVertPos);
					vertices[i*longitude + j + 1].position.y = DirectX::XMVectorGetY(currVertPos);
					vertices[i*longitude + j + 1].position.z = DirectX::XMVectorGetZ(currVertPos);
				}
			}

			vertices[NumSphereVertices - 1].position.x = 0.0f;
			vertices[NumSphereVertices - 1].position.y = 0.0f;
			vertices[NumSphereVertices - 1].position.z = -1.0f;

			for (auto &vertex : vertices)
			{
				DirectX::XMVECTOR norm = { vertex.position.x, vertex.position.y, vertex.position.z, 1.0f };
				norm = DirectX::XMVector3Normalize(norm);
				vertex.normal.x = DirectX::XMVectorGetX(norm);
				vertex.normal.y = DirectX::XMVectorGetY(norm);
				vertex.normal.z = DirectX::XMVectorGetZ(norm);

				vertex.textureCoordinate.x = atan2f(DirectX::XMVectorGetX(norm), DirectX::XMVectorGetZ(norm)) / (2 * DirectX::XM_PI) + 0.5f;
				vertex.textureCoordinate.y = 0.5f - asin(DirectX::XMVectorGetY(norm)) / DirectX::XM_PI;

				vertex.color.x = 0.00f;
				vertex.color.y = 0.00f;
				vertex.color.z = 0.00f;
				vertex.color.w = 1.0f;
			}

			std::vector<unsigned short> indices(NumSphereFaces * 3);

			int k = 0;
			for (unsigned short l = 0; l < longitude - 1; ++l)
			{
				indices[k] = 0;
				indices[k + 1] = l + 1;
				indices[k + 2] = l + 2;
				k += 3;
			}

			indices[k] = 0;
			indices[k + 1] = longitude;
			indices[k + 2] = 1;
			k += 3;

			for (unsigned short i = 0; i < latitude - 3; ++i)
			{
				for (unsigned short j = 0; j < longitude - 1; ++j)
				{
					indices[k] = i*longitude + j + 1;
					indices[k + 1] = i*longitude + j + 2;
					indices[k + 2] = (i + 1)*longitude + j + 1;

					indices[k + 3] = (i + 1)*longitude + j + 1;
					indices[k + 4] = i*longitude + j + 2;
					indices[k + 5] = (i + 1)*longitude + j + 2;

					k += 6; // next quad
				}

				indices[k] = (i*longitude) + longitude;
				indices[k + 1] = (i*longitude) + 1;
				indices[k + 2] = ((i + 1)*longitude) + longitude;

				indices[k + 3] = ((i + 1)*longitude) + longitude;
				indices[k + 4] = (i*longitude) + 1;
				indices[k + 5] = ((i + 1)*longitude) + 1;

				k += 6;
			}

			for (unsigned short l = 0; l < longitude - 1; ++l)
			{
				indices[k] = NumSphereVertices - 1;
				indices[k + 1] = (NumSphereVertices - 1) - (l + 1);
				indices[k + 2] = (NumSphereVertices - 1) - (l + 2);
				k += 3;
			}

			indices[k] = NumSphereVertices - 1;
			indices[k + 1] = (NumSphereVertices - 1) - longitude;
			indices[k + 2] = NumSphereVertices - 2;

			return MeshBase::CreateAsync(device, vertices, indices);
		}

		// Loading from a file
		virtual concurrency::task<void> LoadAsync(_In_ ID3D11Device2* device, const std::wstring&) override
		{
			DebugPrint(std::string("\t Sphere<VertexPosition, unsigned short>::CreateAsync() ...\n"));

			std::vector <DirectX::VertexPositionNormalColorTexture> vertices;
			std::vector<unsigned short> indices;

			// Loading from a file

			return MeshBase::CreateAsync(device, vertices, indices);
		}

		// Creating from memory
		virtual concurrency::task<void> CreateAsync(_In_ ID3D11Device2* device, const std::vector<char>&)
		{
			DebugPrint(std::string("\t Sphere<VertexPosition, unsigned>::CreateAsync() ...\n"));

			std::vector<DirectX::VertexPositionNormalColorTexture> vertices;
			std::vector<unsigned short> indices;

			// Extract (parse) vertices from memory

			return MeshBase::CreateAsync(device, vertices, indices);
		}
	};
}
