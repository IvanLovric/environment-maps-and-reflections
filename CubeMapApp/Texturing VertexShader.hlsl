

//
//	Texturing VertexShader.hlsl
//  Defining a sample vertex shader
//
//  � 2015 Vanity DirectX 11.2 Engine (VXE). Zoraja Consulting d.o.o. All rights reserved.
//

cbuffer WorldTransforms : register(b0)
{
	matrix World;
	matrix Dlrow;
};

cbuffer ViewTransforms : register(b1)
{
	matrix View;
};

cbuffer ProjectionTransforms : register(b2)
{
	matrix Projection;
};

// Per-vertex data used as input to the vertex shader.
struct VertexShaderInput
{
	float3 pos : SV_POSITION;
	float3 norm : NORMAL;
	float4 color: COLOR;
	float2 tex : TEXCOORD0;
};

// Per-pixel color data passed through the pixel shader.
struct VertexShaderOutput
{
	float4 pos : SV_POSITION;
	float3 norm : NORMAL;
	float2 tex : TEXCOORD0;
	float4 color : COLOR;
	float4 pos2 : POSITION;
};

VertexShaderOutput main(VertexShaderInput input)
{
	VertexShaderOutput output;
	float4 pos = float4(input.pos, 1.0f);
	pos = mul(pos, World);
	output.pos2 = pos;
	pos = mul(pos, View);
	pos = mul(pos, Projection);
	output.pos = pos;
	

	// Pass through the texture coordinate without modification
	output.tex = input.tex;
	output.norm = input.norm;
	output.color = input.color;	

	return output;
}