
#pragma once

//
//	Environment.h inspired by Cubes.h
//  Defining cube model for environment mapping 
//
//  � 2015 Vanity DirectX 11.2 Engine (VXE). Zoraja Consulting d.o.o. All rights reserved.
//

#include "pch.h"
#include "..\..\..\Library\Engine\Utilities.h"
#include "..\..\..\Library\Engine\Models\Mesh Base.h"

namespace vxe {

	template <typename T, typename U> class Environment : public MeshBase<T, U> {};

	template <>
	class Environment<DirectX::VertexPositionNormalColorTexture, unsigned short> : public MeshBase<DirectX::VertexPositionNormalColorTexture, unsigned short>{

	private:
		float scale;

	public:
		Environment(float scale) :scale(scale) {} 

		// Vertices are hardcoded
		virtual concurrency::task<void> CreateAsync(_In_ ID3D11Device2* device) override
		{
			DebugPrint(std::string("\t Environment<VertexPositionNormalColorTexture, unsigned short>::CreateAsync() ...\n"));

			// Right-handed
			std::vector<DirectX::VertexPositionNormalColorTexture> vertices = {

				// +Y (top face)
				DirectX::VertexPositionNormalColorTexture{ DirectX::XMFLOAT3(-1.0f, 1.0f, -1.0f), DirectX::XMFLOAT3(0.0f, -1.0f, 0.0f),DirectX::XMFLOAT4(0.1f, 0.1f, 0.1f, 0.1f) ,DirectX::XMFLOAT2(0.25f, 0.0f) },
				DirectX::VertexPositionNormalColorTexture{ DirectX::XMFLOAT3(1.0f, 1.0f, -1.0f), DirectX::XMFLOAT3(0.0f, -1.0f, 0.0f), DirectX::XMFLOAT4(0.1f, 0.1f, 0.1f, 0.1f), DirectX::XMFLOAT2(0.5f, 0.0f) },
				DirectX::VertexPositionNormalColorTexture{ DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f), DirectX::XMFLOAT3(0.0f, -1.0f, 0.0f), DirectX::XMFLOAT4(0.1f, 0.1f, 0.1f, 0.1f), DirectX::XMFLOAT2(0.5f, 1.0f / 3) },
				DirectX::VertexPositionNormalColorTexture{ DirectX::XMFLOAT3(-1.0f, 1.0f, 1.0f), DirectX::XMFLOAT3(0.0f, -1.0f, 0.0f), DirectX::XMFLOAT4(0.1f, 0.1f, 0.1f, 0.1f), DirectX::XMFLOAT2(0.25f, 1.0f / 3) },

				// -Y (bottom face)
				DirectX::VertexPositionNormalColorTexture{ DirectX::XMFLOAT3(-1.0f, -1.0f, 1.0f), DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f), DirectX::XMFLOAT4(0.1f, 0.1f, 0.1f, 0.1f), DirectX::XMFLOAT2(0.25f, 2.0f / 3) },
				DirectX::VertexPositionNormalColorTexture{ DirectX::XMFLOAT3(1.0f, -1.0f, 1.0f), DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f), DirectX::XMFLOAT4(0.1f, 0.1f, 0.1f, 0.1f), DirectX::XMFLOAT2(0.5f, 2.0f / 3) },
				DirectX::VertexPositionNormalColorTexture{ DirectX::XMFLOAT3(1.0f, -1.0f, -1.0f), DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f), DirectX::XMFLOAT4(0.1f, 0.1f, 0.1f, 0.1f), DirectX::XMFLOAT2(0.5f, 1.0f) },
				DirectX::VertexPositionNormalColorTexture{ DirectX::XMFLOAT3(-1.0f, -1.0f, -1.0f), DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f), DirectX::XMFLOAT4(0.1f, 0.1f, 0.1f, 0.1f), DirectX::XMFLOAT2(0.25f, 1.0f) },

				// +X (right face)
				DirectX::VertexPositionNormalColorTexture{ DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f), DirectX::XMFLOAT3(-1.0f, 0.0f, 0.0f), DirectX::XMFLOAT4(0.1f, 0.1f, 0.1f, 0.1f), DirectX::XMFLOAT2(0.5f, 1.0f / 3) },
				DirectX::VertexPositionNormalColorTexture{ DirectX::XMFLOAT3(1.0f, 1.0f, -1.0f), DirectX::XMFLOAT3(-1.0f, 0.0f, 0.0f), DirectX::XMFLOAT4(0.1f, 0.1f, 0.1f, 0.1f), DirectX::XMFLOAT2(0.75f, 1.0f / 3) },
				DirectX::VertexPositionNormalColorTexture{ DirectX::XMFLOAT3(1.0f, -1.0f, -1.0f), DirectX::XMFLOAT3(-1.0f, 0.0f, 0.0f), DirectX::XMFLOAT4(0.1f, 0.1f, 0.1f, 0.1f), DirectX::XMFLOAT2(0.75f, 2.0f / 3) },
				DirectX::VertexPositionNormalColorTexture{ DirectX::XMFLOAT3(1.0f, -1.0f, 1.0f), DirectX::XMFLOAT3(-1.0f, 0.0f, 0.0f), DirectX::XMFLOAT4(0.1f, 0.1f, 0.1f, 0.1f), DirectX::XMFLOAT2(0.5f, 2.0f / 3) },

				// -X (left face)
				DirectX::VertexPositionNormalColorTexture{ DirectX::XMFLOAT3(-1.0f, 1.0f, -1.0f), DirectX::XMFLOAT3(1.0f, 0.0f, 0.0f), DirectX::XMFLOAT4(0.1f, 0.1f, 0.1f, 0.1f), DirectX::XMFLOAT2(0.0f, 1.0f / 3) },
				DirectX::VertexPositionNormalColorTexture{ DirectX::XMFLOAT3(-1.0f, 1.0f, 1.0f), DirectX::XMFLOAT3(1.0f, 0.0f, 0.0f), DirectX::XMFLOAT4(0.1f, 0.1f, 0.1f, 0.1f), DirectX::XMFLOAT2(0.25f, 1.0f / 3) },
				DirectX::VertexPositionNormalColorTexture{ DirectX::XMFLOAT3(-1.0f, -1.0f, 1.0f), DirectX::XMFLOAT3(1.0f, 0.0f, 0.0f), DirectX::XMFLOAT4(0.1f, 0.1f, 0.1f, 0.1f), DirectX::XMFLOAT2(0.25f, 2.0f / 3) },
				DirectX::VertexPositionNormalColorTexture{ DirectX::XMFLOAT3(-1.0f, -1.0f, -1.0f), DirectX::XMFLOAT3(1.0f, 0.0f, 0.0f), DirectX::XMFLOAT4(0.1f, 0.1f, 0.1f, 0.1f), DirectX::XMFLOAT2(0.0f, 2.0f / 3) },

				// +Z (front face)
				DirectX::VertexPositionNormalColorTexture{ DirectX::XMFLOAT3(-1.0f, 1.0f, 1.0f), DirectX::XMFLOAT3(0.0f, 0.0f, -1.0f), DirectX::XMFLOAT4(0.1f, 0.1f, 0.1f, 0.1f), DirectX::XMFLOAT2(0.25f, 1.0f / 3) },
				DirectX::VertexPositionNormalColorTexture{ DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f), DirectX::XMFLOAT3(0.0f, 0.0f, -1.0f), DirectX::XMFLOAT4(0.1f, 0.1f, 0.1f, 0.1f), DirectX::XMFLOAT2(0.5f, 1.0f / 3) },
				DirectX::VertexPositionNormalColorTexture{ DirectX::XMFLOAT3(1.0f, -1.0f, 1.0f), DirectX::XMFLOAT3(0.0f, 0.0f, -1.0f), DirectX::XMFLOAT4(0.1f, 0.1f, 0.1f, 0.1f), DirectX::XMFLOAT2(0.5f, 2.0f / 3) },
				DirectX::VertexPositionNormalColorTexture{ DirectX::XMFLOAT3(-1.0f, -1.0f, 1.0f), DirectX::XMFLOAT3(0.0f, 0.0f, -1.0f), DirectX::XMFLOAT4(0.1f, 0.1f, 0.1f, 0.1f), DirectX::XMFLOAT2(0.25f, 2.0f / 3) },

				// -Z (back face)
				DirectX::VertexPositionNormalColorTexture{ DirectX::XMFLOAT3(1.0f, 1.0f, -1.0f), DirectX::XMFLOAT3(0.0f, 0.0f, 1.0f), DirectX::XMFLOAT4(0.1f, 0.1f, 0.1f, 0.1f), DirectX::XMFLOAT2(0.75f, 1.0f / 3) },
				DirectX::VertexPositionNormalColorTexture{ DirectX::XMFLOAT3(-1.0f, 1.0f, -1.0f), DirectX::XMFLOAT3(0.0f, 0.0f, 1.0f), DirectX::XMFLOAT4(0.1f, 0.1f, 0.1f, 0.1f), DirectX::XMFLOAT2(1.0f, 1.0f / 3) },
				DirectX::VertexPositionNormalColorTexture{ DirectX::XMFLOAT3(-1.0f, -1.0f, -1.0f), DirectX::XMFLOAT3(0.0f, 0.0f, 1.0f), DirectX::XMFLOAT4(0.1f, 0.1f, 0.1f, 0.1f), DirectX::XMFLOAT2(1.0f, 2.0f / 3) },
				DirectX::VertexPositionNormalColorTexture{ DirectX::XMFLOAT3(1.0f, -1.0f, -1.0f), DirectX::XMFLOAT3(0.0f, 0.0f, 1.0f), DirectX::XMFLOAT4(0.1f, 0.1f, 0.1f, 0.1f), DirectX::XMFLOAT2(0.75f, 2.0f / 3) }
			};

			std::vector<unsigned short> indices = {
				0, 1, 2,		// side 1
				0, 2, 3,
				4, 5, 6,		// side 2
				4, 6, 7,
				8, 9, 10,		// side 3
				8, 10, 11,
				12, 13, 14,		// side 4
				12, 14, 15,
				16, 17, 18,		// side 5
				16, 18, 19,
				20, 21, 22,		// side 6
				20, 22, 23
			};

			// Applied transforms
			for (auto &vertex : vertices) 
			{
				vertex.position.x *= scale;
				vertex.position.y *= scale;
				vertex.position.z *= scale;
			}

			return MeshBase::CreateAsync(device, vertices, indices);
		}

		// Loading from a file
		virtual concurrency::task<void> LoadAsync(_In_ ID3D11Device2* device, const std::wstring&) override
		{
			DebugPrint(std::string("\t Environment<VertexPositionNormalColorTexture, unsigned short>::CreateAsync() ...\n"));

			std::vector <DirectX::VertexPositionNormalColorTexture> vertices;
			std::vector<unsigned short> indices;

			// Loading from a file

			return MeshBase::CreateAsync(device, vertices, indices);
		}

		// Creating from memory
		virtual concurrency::task<void> CreateAsync(_In_ ID3D11Device2* device, const std::vector<char>&)
		{
			DebugPrint(std::string("\t Environment<VertexPositionNormalColorTexture, unsigned>::CreateAsync() ...\n"));

			std::vector<DirectX::VertexPositionNormalColorTexture> vertices;
			std::vector<unsigned short> indices;

			// Extract (parse) vertices from memory

			return MeshBase::CreateAsync(device, vertices, indices);
		}
	};

}